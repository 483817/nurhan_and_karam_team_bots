package nl.saxion;

import java.awt.*;
import java.io.Serializable;

public class Colors implements Serializable {
    private static final long serialVersionUID = 1L;

    public Color body;
    public Color gun;
    public Color radar;
    public Color scan;
    public Color bullet;
}
