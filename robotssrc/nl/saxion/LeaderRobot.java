package nl.saxion;

import robocode.*;
import sampleteam.RobotColors;

import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

public class LeaderRobot extends TeamRobot {

    private ArrayList<Helper> helpers;
    private boolean isLeader = true;

    public void run() {
        // add the team mates to array. it will be useful to make comparision in te-
        // rms of which tank has the highest energy to get to be the next leader
        System.out.println(getName());
        helpers = new ArrayList<>();
        String[] teamMates = getTeammates();
        for (int i = 0; i < teamMates.length; i++) {
            helpers.add(new Helper(teamMates[i], 100.0));
        }


        // Assign a the first tank to be a leader as an initialize
        // if this tank got attacked and lost energy, it will recalculate
        // the amount of energy for each other tanks, and see the highest
        // tank's energy to be the initialized leader, and it will be activated
        // once the leader is dead.
        // see function getHighestEnergy() in this file
        // see function onMessageReceived() in EastRobot.java in the same
        // directory.
        try {
            broadcastMessage(helpers.get(0).getName());
        } catch (IOException e) {
            e.printStackTrace();
        }


        // Normal behavior
        while (true) {
            setTurnRadarRight(360);
            //ahead(100);
            //back(100);
            execute();

        }
    }


    // function to decide what to do when detecting a robot
    public void onScannedRobot(ScannedRobotEvent e) {
        // ignore the robot
        if (isTeammate(e.getName())) {
            return;
        }
        // get enemy bearing
        double enemyBearing = this.getHeading() + e.getBearing();
        // get enemy's robot position
        double robotX = getX() + e.getDistance() * Math.sin(Math.toRadians(enemyBearing));
        double robotY = getY() + e.getDistance() * Math.cos(Math.toRadians(enemyBearing));

        try {
            // Send enemy position to teammates
            broadcastMessage(new Helper(robotX, robotY));
        } catch (IOException ex) {
            out.println("Something wrong with sending enemies locations! ");
            ex.printStackTrace(out);
        }
    }

    @Override
    public void onMessageReceived(MessageEvent event) {
        // if energy is sent via a message,
        if (event.getMessage() instanceof Double) {
            //update the array list of team mates with the current energies
            for (Helper helper : helpers) {
                if (helper.getName().equals(event.getSender())) {
                    helper.setEnergy((Double) event.getMessage());
                    System.out.println(helper.getName() + " energy updated to: " + helper.getEnergy());
                }
            }
            Helper newLeader = getHighestEnergy();
            System.out.println("\n<----------------------");
            for (Helper helper : helpers) {
                System.out.println(helper.getName() + ", " + helper.getEnergy());
            }
            System.out.println("highest is: " + newLeader.getName() + ", " + newLeader.getEnergy());
            System.out.println("---------------------->\n");
            try {
                broadcastMessage(newLeader.getName());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    // get the highest energy among team mates
    private Helper getHighestEnergy() {
        Helper tempHelper = new Helper(helpers.get(0).getName(), helpers.get(0).getEnergy());
        for (Helper helper : helpers) {
            if (helper.getEnergy() > tempHelper.getEnergy()) {
                tempHelper = helper;
            }
        }
        return tempHelper;
    }


}
