package nl.saxion;

import java.io.Serializable;

public class Helper implements Serializable {
    private static final long serialVersionUID = 1L;

    private double x = 0.0;
    private double y = 0.0;
    private String name;
    private Double energy;

    public Helper() {

    }


    public Helper(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Helper(String name, Double energy) {
        this.name = name;
        this.energy = energy;
    }


    public Double getEnergy() {
        return energy;
    }

    public String getName() {
        return name;
    }

    public void setEnergy(Double energy) {
        this.energy = energy;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }
}
