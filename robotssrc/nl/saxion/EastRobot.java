package nl.saxion;

import robocode.*;
import robocode.Event;
import sampleteam.Point;
import sampleteam.RobotColors;

import java.awt.*;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Vector;

import static robocode.util.Utils.normalRelativeAngleDegrees;

public class EastRobot extends TeamRobot {

    double moveAmount;
    private boolean isLeader = false;
    private boolean leaderRobot = true;
    private boolean alreadyOnWall = false;
    //Variables in order to know if the robot previously moved right or left.
    private boolean moveLeft = false;
    private boolean moveRight = false;


    public void run() {
        moveAmount = Math.max(getBattleFieldWidth(), getBattleFieldHeight());
        setRobotColor();
        //System.out.println(getName());
        stickToWall();
        //Positioning the robot to face the battlefield.
        alreadyOnWall = true;
        turnRight(90);
        turnGunRight(90);
        //Moving from right to left.
        while (true) {
            if (isLeader && !leaderRobot) {
                while (true) {
                    setTurnRadarRight(360);
                    execute();
                    positioning();
                }
            } else {
                moveAmount = Math.max(getBattleFieldWidth(), getBattleFieldHeight());
                positioning();
            }
        }
    }

    @Override
    public void onMessageReceived(MessageEvent event) {
        if (event.getMessage() instanceof String) {
            if (event.getMessage().equals(getName())) {
                System.out.println("i'm the coming leader");
                isLeader = true;
            } else {
                isLeader = false;
                System.out.println("i'm not the coming leader");

            }
        }
        // Fire at a point
        if (event.getMessage() instanceof Helper) {
            Helper help = (Helper) event.getMessage();
            // Calculate x and y to target
            double dx = help.getX() - this.getX();
            double dy = help.getY() - this.getY();
            // Calculate angle to target
            double theta = Math.toDegrees(Math.atan2(dx, dy));
            // Turn gun head to target
            getVelocity();
            setMaxVelocity(8);
            turnGunRight(normalRelativeAngleDegrees(theta - getGunHeading()));
            // fire
            fire(3);
        }
    }


    @Override
    public void onHitByBullet(HitByBulletEvent event) {
        System.out.println("I got a hit");
        try {
            broadcastMessage(getEnergy());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

     //function to decide what to do when detecting a robot  | this function is fine
    public void onScannedRobot(ScannedRobotEvent e) {
        if (isLeader && !leaderRobot) {
            // ignore the team robot
            if (isTeammate(e.getName())) {
                return;
            }
            // get enemy bearing
            double enemyBearing = this.getHeading() + e.getBearing();
            // get enemy's robot position
            double robotX = getX() + e.getDistance() * Math.sin(Math.toRadians(enemyBearing));
            double robotY = getY() + e.getDistance() * Math.cos(Math.toRadians(enemyBearing));
            try {
                // Send enemy position to teammates
                broadcastMessage(new Helper(robotX, robotY));
            } catch (IOException ex) {
                out.println("Something wrong with sending enemies locations ");
                ex.printStackTrace(out);
            }
        }
    }


    @Override
    public void onRobotDeath(RobotDeathEvent event) {
        if (event.getName().contains("LeaderRobot")) {
            System.out.println("LEADER IS DEAD GUYS");
            if (isLeader) {
                System.out.println(getName() + " is the new leader");
            }
            leaderRobot = false;
        }
    }

    //Positioning
    private void positioning() {

        //Moving sideways
        while (!isLeader) {


            moveRight = true;
            ahead(moveAmount);

            moveLeft = true;
            back(moveAmount);
            execute();

        }
    }

    public void onHitRobot(HitRobotEvent event) {
        if (!alreadyOnWall) //If the robot hits another robot and is not on the wall it will continue to move until it reaches the wall.
        {
            back(100);
            turnRight(90);
            ahead(100);
            stickToWall();
        } else if (alreadyOnWall) //If the robot hits another robot in the corners or along the wall it will go on the opposite direction. Remaining on the wall.
        {
            if (moveRight) {
                moveRight = false;
                back(moveAmount);
            } else if (moveLeft) {
                moveLeft = false;
                ahead(moveAmount);
            }
        }
    }


    //Method that make the robot stick to the east wall.
    private void stickToWall() {
        double facingPosition = getHeading();
        calculateRotation(facingPosition);

        ahead(moveAmount);
    }

    private void calculateRotation(double facingPosition) {
        if (facingPosition >= 90) {
            turnLeft(facingPosition - 90);
        } else if (facingPosition <= 90) {
            turnRight(90 - facingPosition);
        }
    }

    private void setRobotColor()
    {
        RobotColors robotColor = new RobotColors();

        robotColor.bodyColor = Color.magenta;
        robotColor.gunColor = Color.white;
        robotColor.radarColor = Color.magenta;
        robotColor.scanColor = Color.magenta;
        robotColor.bulletColor = Color.magenta;

        // Set the color of the robot
        setBodyColor(robotColor.bodyColor);
        setGunColor(robotColor.gunColor);
        setRadarColor(robotColor.radarColor);
        setScanColor(robotColor.scanColor);
        setBulletColor(robotColor.bulletColor);

    }



}
